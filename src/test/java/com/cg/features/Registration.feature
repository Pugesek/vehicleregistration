Feature: Vehicle Registration 

Scenario: Successful Registration after filling out all fields 
	Given User is on Vehicle Registration Form page 
	And Title of page is correct
	Then Select the title field with the correct default
	When Ownername is blank or wrong
	Then Alert1 Appears
	Then Enter a correct ownername
	When Gender is not selected
	Then Alert2 Appears
	Then Enter a correct gender
	When Address is wrong
	Then Alert3 Appears
	Then Enter a correct address
	When City is blank or wrong
	Then Alert4 Appears
	Then Enter a correct City
	When State is blank or wrong
	Then Alert5 Appears
	Then Enter a correct State
	When County is default
	Then Alert6 Appears
	Then Choose a correct Country
	When Zip code is empty or wrong
	Then Alert7 Appears
	Then Enter a correct zip code
	When Vehicle type is not chosen
	Then Alert8 Appears
	Then Select a vehicle type
	When Fueltype is not chosen
	Then Alert9 Appears
	Then Select a fuel type
	When year of mfg is default
	Then Alert10 Appears
	Then Select a year
	Then Submit