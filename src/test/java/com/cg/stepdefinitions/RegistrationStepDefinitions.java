package com.cg.stepdefinitions;

import io.cucumber.java.en.And;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.cg.vehicle.VehicleRegistrationPage;

public class RegistrationStepDefinitions {

	WebDriver driver;
	VehicleRegistrationPage vrp;
	
	private String getAlertText() {
		Alert alert = driver.switchTo().alert();
		String output = alert.getText();
		alert.accept();
		return output;
	}
    @Given("^User is on Vehicle Registration Form page$")
    public void user_is_on_vehicle_registration_form_page() throws Throwable {
    	System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver_win32\\chromedriver.exe");
		
    	driver = new ChromeDriver();

		driver.get("file:///C:/Users/username/Documents/SDETTraining/VehicleRegistration.html");
    }
    
    @And("^Title of page is correct$")
    public void title_of_page_is_correct() throws Throwable {
        vrp = new VehicleRegistrationPage(driver);
        
        Assert.assertEquals(true, driver.getTitle().equals("Welcome to VehicleRegistration"));
        
        Assert.assertEquals(true, vrp.header.getText().equals("Vehicle Registration Form"));
    }
    
    @Then("^Select the title field with the correct default$")
    public void select_the_title_field_with_the_correct_default() throws Throwable {
    	vrp.submit.click();
    	
    	Assert.assertTrue(this.getAlertText().equals("Select title from the list"));
    	
    	vrp.title.selectByIndex(1);
    	
    	Assert.assertTrue(vrp.title.getFirstSelectedOption().getText().equals("Mr."));
    	
    }
    
 
    @When("^Ownername is blank or wrong$")
    public void ownername_is_blank_or_wrong() throws Throwable {
        vrp.submit.click();
    }
    @Then("^Alert1 Appears$")
    public void alert1_appears() throws Throwable {
    	Assert.assertTrue(this.getAlertText().equals("Owner Name should not be empty and must contain alphabets with in the range of 5 to 20"));
    }
    @Then("^Enter a correct ownername$")
    public void enter_a_correct_ownername() throws Throwable {
    	try {
    		vrp.ownername.sendKeys("E");
            vrp.submit.click();
        	Assert.assertTrue(this.getAlertText().equals("Owner Name should not be empty and must contain alphabets with in the range of 5 to 20"));
        	vrp.ownername.clear();
        	vrp.ownername.sendKeys("user111");
        	//Assert.assertTrue(vrp.ownername.getText().equals("erikPugesek"));
    	} catch(AssertionError e) {
    		System.out.println(e);
    	}
        
    }
    
    @When("^Gender is not selected$")
    public void gender_is_not_selected() throws Throwable {
    	vrp.submit.click();
    }
    @Then("^Alert2 Appears$")
    public void alert2_appears() throws Throwable {

    	Assert.assertTrue(this.getAlertText().equals("Please Select gender"));
    }
    @Then("^Enter a correct gender$")
    public void enter_a_correct_gender() throws Throwable {
        vrp.maleRadio.click();
    }
    
    
    @When("^Address is wrong$")
    public void address_is_wrong() throws Throwable {
    	vrp.submit.click();
    }
    @Then("^Alert3 Appears$")
    public void alert3_appears() throws Throwable {
    	Assert.assertTrue(this.getAlertText().equals("address should not be empty and must be alphanumeric with in the range of 5 to 20"));
    }
    @Then("^Enter a correct address$")
    public void enter_a_correct_address() throws Throwable {
        vrp.address.sendKeys("12345NewDisneyLand");
    }
    
    

    @When("^City is blank or wrong$")
    public void city_is_blank_or_wrong() throws Throwable {
        vrp.submit.click();
    }
    @Then("^Alert4 Appears$")
    public void alert4_appears() throws Throwable {
    	Assert.assertTrue(this.getAlertText().equals("city should not be empty and must have alphabet characters only"));
    }
    @Then("^Enter a correct City$")
    public void enter_a_correct_city() throws Throwable {
        vrp.city.sendKeys("SomeCityName");
    }
    
    

    @When("^State is blank or wrong$")
    public void state_is_blank_or_wrong() throws Throwable {
        vrp.submit.click();
    }
    @Then("^Alert5 Appears$")
    public void alert5_appears() throws Throwable {
    	Assert.assertTrue(this.getAlertText().equals("State should not be empty and must have alphabet characters only"));
    }
    @Then("^Enter a correct State$")
    public void enter_a_correct_state() throws Throwable {
        vrp.state.sendKeys("SomeState");
        Assert.assertEquals("SomeState", vrp.state.getAttribute("value"));
    }
    
    

    @When("^County is default$")
    public void county_is_default() throws Throwable {
        vrp.submit.click();
    }
    @Then("^Alert6 Appears$")
    public void alert6_appears() throws Throwable {
    	Assert.assertTrue(this.getAlertText().equals("Select the country from the list"));
    }
    @Then("^Choose a correct Country$")
    public void choose_a_correct_country() throws Throwable {
        vrp.country.selectByIndex(1);
    }
    
    

    @When("^Zip code is empty or wrong$")
    public void zip_code_is_empty_or_wrong() throws Throwable {
        vrp.submit.click();
    }
    @Then("^Alert7 Appears$")
    public void alert7_appears() throws Throwable {
    	Assert.assertTrue(this.getAlertText().equals("ZIP code should not be empty and must have 6 numeric characters only"));
    }
    @Then("^Enter a correct zip code$")
    public void enter_a_correct_zip_code() throws Throwable {
        vrp.zip.sendKeys("123456");
    }
    
    

    @When("^Vehicle type is not chosen$")
    public void vehicle_type_is_not_chosen() throws Throwable {
        vrp.submit.click();
    }
    @Then("^Alert8 Appears$")
    public void alert8_appears() throws Throwable {
    	Assert.assertTrue(this.getAlertText().equals("Please Select Vehicle type"));
    }

    @Then("^Select a vehicle type$")
    public void select_a_vehicle_type() throws Throwable {
        vrp.fourWheelerRadio.click();
    }
    
    

    @When("^Fueltype is not chosen$")
    public void fueltype_is_not_chosen() throws Throwable {
        vrp.submit.click();
    }
    @Then("^Alert9 Appears$")
    public void alert9_appears() throws Throwable {
    	Assert.assertTrue(this.getAlertText().equals("Please Select fuel type"));

    }

    @Then("^Select a fuel type$")
    public void select_a_fuel_type() throws Throwable {
        vrp.petrolRadio.click();
    }
    
    

    @When("^year of mfg is default$")
    public void year_of_mfg_is_default() throws Throwable {
        vrp.submit.click();
    }
    @Then("^Alert10 Appears$")
    public void alert10_appears() throws Throwable {
    	Assert.assertTrue(this.getAlertText().equals("Select mfg year from the list"));

    }
    @Then("^Select a year$")
    public void select_a_year() throws Throwable {
        vrp.mfg.selectByIndex(1);
    }
    

    
    @Then("^Submit$")
    public void submit() throws Throwable {
        vrp.submit.click();
        Assert.assertTrue(this.getAlertText().equals("You are Succesfully registered your vehicle"));
        driver.close();
    }

}