package com.cg.gitlabNG;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.cg.gitlab.GitlabSignIn;


public class GitlabTestNG {
	WebDriver driver;
	GitlabSignIn glsi;
	@BeforeTest
	public void setBaseURL() {
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://gitlab.com/users/sign_in");
		glsi = new GitlabSignIn(driver);
	}
	
	@Test
	public void login() {
		Assert.assertEquals("https://gitlab.com/users/sign_in", driver.getCurrentUrl());
		
		glsi.username.sendKeys("Username");
		glsi.password.sendKeys("UserPassword");
		
		Assert.assertEquals("Username", glsi.username.getAttribute("value"));
		
		glsi.signInButton.click();
	}
	
	@AfterTest
	public void checkPage() {
		Assert.assertEquals("https://gitlab.com/", driver.getCurrentUrl());
		driver.close();
	}
}
