package com.cg.vehicle;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class VehicleRegistrationPage {

	WebDriver driver;
	
	public VehicleRegistrationPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
		
		initSelects(driver);
	}
	
	private void initSelects(WebDriver d) {
		title = new Select(d.findElement(By.name("title")));
		country = new Select(d.findElement(By.name("country")));
		mfg = new Select(d.findElement(By.name("mfg")));
	}
	public Select title;
	public Select country;
	public Select mfg;
	@FindBy(xpath="/html/body/h1")
	public WebElement header;
	
	@FindBy(name="title")
	public WebElement title1;
	
	@FindBy(id="ownername")
	public WebElement ownername;
	
	@FindBy(xpath="/html/body/form/ul/li[6]/input")
	public WebElement maleRadio;
	
	@FindBy(xpath="/html/body/form/ul/li[7]/input")
	public WebElement femaleRadio;
	
	@FindBy(id="address")
	public WebElement address;
	
	@FindBy(id="city")
	public WebElement city;
	
	@FindBy(name="state")
	public WebElement state;
	
	@FindBy(id="zip")
	public WebElement zip;
	
	@FindBy(xpath="/html/body/form/ul/li[19]/input")
	public WebElement fourWheelerRadio;
	
	@FindBy(xpath="/html/body/form/ul/li[20]/input")
	public WebElement twoWheelerRadio;
	
	@FindBy(xpath="/html/body/form/ul/li[22]/input")
	public WebElement petrolRadio;
	
	@FindBy(xpath="/html/body/form/ul/li[23]/input")
	public WebElement dieselRadio;
	
	@FindBy(name="submit")
	public WebElement submit;
	
}
